import React from 'react';
import './Menu.css';
const Menu=props=>{
    return(
        <div className="MainMenu">



            <p className="name">{props.name}</p>
            <button onClick={props.remove}>Less</button>
            <button onClick={props.add}>More</button>

        </div>
    )
};
export default Menu;