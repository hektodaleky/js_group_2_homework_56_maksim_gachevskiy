/**
 * Created by Max on 26.01.2018.
 */
import React from 'react';
const ingredients = {
    cheese: "Cheese",
    salad: "Salad",
    meat: "Meat",
    bacon:"Bacon",
    breadTop: "BreadTop",
    breadBottom: "BreadBottom"


};
const getIngredient = (type) => {
    if (ingredients[type] === ingredients.breadTop) {
        return (<div className={ingredients[type]}>
            <div className="Seeds1"></div>
            <div className="Seeds2"></div>
        </div>);
    }
    else
        return ( <div className={ingredients[type]}></div>);
};
const Ingredient = props => {

    return (
        getIngredient(props.type)
    )
};

export default Ingredient;