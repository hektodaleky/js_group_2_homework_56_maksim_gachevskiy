import React, {Component} from "react";
import "./App.css";
import BurgerBuilder from "./components/BurgerBuilder/BurgerBuilder";
import Menu from "./components/Menu/Menu";

const price={
    bacon: 30,
    cheese: 10,
    salad: 5,
    meat: 50
};


class App extends Component {
    state = {
        ingredients: {
            breadTop: 1,
            bacon: 0,
            cheese: 0,
            salad: 0,
            meat: 0,
            breadBottom: 1
        }

    };

    addIngredient = (prod) => {
        let newIngred = {...this.state.ingredients};
        newIngred[prod]++;
        this.setState({ingredients: newIngred});
    };
    lessIngredient = (prod) => {
        let newIngred = {...this.state.ingredients};
        if (newIngred[prod] > 0)
            newIngred[prod]--;
        this.setState({ingredients: newIngred});
    };

    render() {
        let totalSum=20;
        for(let i in this.state.ingredients){
            if (i === "breadTop" || i === "breadBottom")
                continue;
            totalSum+=this.state.ingredients[i]*price[i];
        };
        return (
            <div>
                <div>
                    <p className="price">
                        {`Total sum: ${totalSum}` }
                    </p>
                </div>
                <div className="Burger">{


                    Object.keys(this.state.ingredients).map((keys,index) => {
                        const arr = [];
                        for (let i = 0; i < this.state.ingredients[keys]; i++) {

                            arr.push(<BurgerBuilder key={Math.random()*Math.random()} type={keys}></BurgerBuilder>)
                        }

                        return arr;
                    })}

                </div>
                <div className="Menu">
                    {
                        Object.keys(this.state.ingredients).map((keys,index) => {
                            if (keys === "breadTop" || keys === "breadBottom")
                                return null;
                            return (<Menu key={Math.random()*Math.random()} name={keys} add={() => {
                                this.addIngredient(keys)
                            }} remove={() => {
                                this.lessIngredient(keys)
                            }}></Menu>)
                        })
                    }
                </div>
            </div>
        )
    }
}

export default App;
